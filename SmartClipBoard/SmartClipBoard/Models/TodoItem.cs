﻿using Newtonsoft.Json;
using System;

namespace SmartClipBoard.Models
{
    public class TodoItem
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty("__createdAt")]
        public DateTime DateCreated { get; set; }
    }
}