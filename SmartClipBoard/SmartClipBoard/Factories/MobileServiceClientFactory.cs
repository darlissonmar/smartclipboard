﻿using Microsoft.WindowsAzure.MobileServices;
using System.Net.Http;

namespace SmartClipBoard.Factories
{
	public static class MobileServiceClientFactory
	{
		public static MobileServiceClient CreateClient()
		{
			return new MobileServiceClient(MobileServiceConfig.AzureMobileServicesUrl,
                MobileServiceConfig.AzureMobileApplicationKey);
		}

        public static MobileServiceClient CreateClient(params HttpMessageHandler[] handlers)
        {
            return new MobileServiceClient(MobileServiceConfig.AzureMobileServicesUrl,
                MobileServiceConfig.AzureMobileApplicationKey, handlers);
        }
    }
}