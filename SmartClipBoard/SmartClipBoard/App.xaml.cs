﻿using System;
using System.Diagnostics;
using System.Resources;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SmartClipBoard.Resources;
using SmartClipBoard.Storage;
using Microsoft.Phone.Notification;
using SmartClipBoard.Models;
using Microsoft.WindowsAzure.MobileServices;

namespace SmartClipBoard
{
    public partial class App : Application
    {
        public static MobileServiceClient MobileService = new MobileServiceClient(
            MobileServiceConfig.AzureMobileServicesUrl, MobileServiceConfig.AzureMobileApplicationKey);
        
        public static MobileServiceUser globalUser;

        public static PhoneApplicationFrame RootFrame { get; private set; }

        public static HttpNotificationChannel CurrentChannel { get; set; }

        public static ProgressIndicator progressIndicator = new ProgressIndicator() { IsIndeterminate = true };


        public App()
        {
            InitializeComponent();

            UserData.TryLoadUser(out globalUser);

            MobileService.CurrentUser = globalUser;

            if (Debugger.IsAttached)
            {
                Application.Current.Host.Settings.EnableFrameRateCounter = true;
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }

        public static void SetProgressIndicator(bool response = false, string text = "Loading itens...")
        {
            progressIndicator.Text = text;

            if (response == true)
            {
                progressIndicator.IsVisible = true;
            }
            else
            {
                progressIndicator.IsVisible = false;
            }
        }
    }
}