﻿using Microsoft.WindowsAzure.MobileServices;
using System.IO.IsolatedStorage;


namespace SmartClipBoard.Storage
{
	public static class UserData
	{
        public static bool TryLoadUser(out MobileServiceUser user)
        {
            object userId, authToken;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("userId") &&
                IsolatedStorageSettings.ApplicationSettings.Contains("authToken"))
            {
                userId = IsolatedStorageSettings.ApplicationSettings["userId"] as string;
                authToken = IsolatedStorageSettings.ApplicationSettings["authToken"] as string;

                user = new MobileServiceUser((string)userId) { MobileServiceAuthenticationToken = (string)authToken };
                return true;
            }
            else
            {
                user = null;
                return false;
            }

        }

        public static void SaveUser(MobileServiceUser user)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if (!settings.Contains("userId") && !settings.Contains("authToken"))
            {
                settings.Add("userId", user.UserId);
                settings.Add("authToken", user.MobileServiceAuthenticationToken);
            }
            else
            {
                settings["userId"] = user.UserId;
                settings["authToken"] = user.MobileServiceAuthenticationToken;
            }

            settings.Save();
        }

        internal static void ClearUserData()
        {
            try
            {
                IsolatedStorageSettings.ApplicationSettings.Clear();
                App.MobileService.Dispose();
                App.globalUser = null;
            }
            catch
            {
                
            }
            
        }
    }
}