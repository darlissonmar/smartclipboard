﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartClipBoard.Models;
using SmartClipBoard.Factories;
using SmartClipBoard.Handlers;
using SmartClipBoard.Storage;
using Microsoft.WindowsAzure.MobileServices;
using System.Windows;

namespace SmartClipBoard.Services
{
    public class ItemService
    {

        public async Task<List<TodoItem>> GetAllItems()
        {
            System.Collections.Generic.List<TodoItem> items = null;

            try
            {
                this.Exchange();

                await App.MobileService.GetTable<TodoItem>().ToListAsync().ContinueWith(t =>
                {
                    if (t.IsFaulted)
                    {
                        System.Diagnostics.Debug.WriteLine("ItemService: Faulted {0}", t.Exception);
                        Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("Connection failed!\n"));
                    }
                    else
                    {
                        items = t.Result;
                        int count = items.Count;
                        if (count == 0)
                        {
                            Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("No record found!\n"));
                        }
                    }
                });
            }
            catch (System.Net.WebException)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("\nConnection failed!\n"));
            }

            return items;
        }

        private void Exchange()
        {
            try
            {
                AuthHandler authHandler = new AuthHandler(UserData.SaveUser);

                MobileServiceClient client = MobileServiceClientFactory.CreateClient(authHandler);

                authHandler.Client = client;

                if (UserData.TryLoadUser(out App.globalUser))
                {
                    client.CurrentUser = App.globalUser;
                }

                App.MobileService = client;
            }
            catch (InvalidOperationException)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("Error on Login!\n"));
            }
        }
    }
}