﻿using Caliburn.Micro;
using System;
using System.Windows.Threading;
using System.Windows;
using SmartClipBoard.Storage;
using SmartClipBoard.Utils;
using Microsoft.Phone.Notification;
using Microsoft.WindowsAzure.MobileServices;
using SmartClipBoard.Models;

namespace SmartClipBoard.ViewModels
{
    public class SplashScreenViewModel : Screen
    {
        INavigationService navigationService;

        DispatcherTimer timerSplash;

        public SplashScreenViewModel(INavigationService NavigationService)
        {
            this.navigationService = NavigationService;
            timerSplash = new DispatcherTimer();
            timerSplash.Interval = new TimeSpan(0, 0, 2);
            timerSplash.Tick += timerSplash_Tick;
            timerSplash.Start();
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            NetworkInformationUtility.GetNetworkTypeCompleted += GetNetworkTypeCompleted;
            NetworkInformationUtility.GetNetworkTypeAsync(3000 /*timeout in ms*/);
        }
        void timerSplash_Tick(object sender, EventArgs e)
        {
            (sender as DispatcherTimer).Stop();

            LoginScreenViewModel.navigate = true;

            if (UserData.TryLoadUser(out App.globalUser))
            {
                navigationService.UriFor<MainViewModel>().Navigate();
            }
            else
            {
                navigationService.UriFor<LoginScreenViewModel>().Navigate();
            }
        }

        private void GetNetworkTypeCompleted(object sender, NetworkTypeEventArgs networkTypeEventArgs)
        {
            string message;

            if (!networkTypeEventArgs.HasInternet)
            {
                message = "At the moment the device does not have internet access. Check your data connection and try again later.";
                
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (MessageBox.Show(message, "Attention", MessageBoxButton.OK) == MessageBoxResult.OK)
                    {
                        Application.Current.Terminate();
                    }
                });
            }

            AcquirePushChannel();
        }

        private async void AcquirePushChannel()
        {
            App.CurrentChannel = HttpNotificationChannel.Find("MyPushChannel");

            if (App.CurrentChannel == null)
            {
                App.CurrentChannel = new HttpNotificationChannel("MyPushChannel");
                App.CurrentChannel.Open();
                //App.CurrentChannel.BindToShellTile();
                //App.CurrentChannel.BindToShellToast();
            }

            IMobileServiceTable<Registrations> registrationsTable = App.MobileService.GetTable<Registrations>();

            var registration = new Registrations { Handle = App.CurrentChannel.ChannelUri.ToString() };

            await registrationsTable.InsertAsync(registration);
        }
    }
}
