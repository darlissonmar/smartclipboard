﻿using Caliburn.Micro;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Threading;
using SmartClipBoard.Views;
using SmartClipBoard.Services;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.Phone.Controls;

namespace SmartClipBoard.ViewModels
{
    public class LoginScreenViewModel : Screen
    {
        INavigationService navigationService;

        LoginScreenView view;
        
        public static bool navigate;

        public LoginScreenViewModel(INavigationService NavigationService)
        {
            this.navigationService = NavigationService;            
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            this.view = GetView() as LoginScreenView;
            this.view.LoginButton.Click += Login;
            navigationService.RemoveBackEntry();
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
        }
       
        protected override void OnViewReady(object view)
        {
            base.OnViewReady(view);
        }

        void Login(object sender, EventArgs events)
        {
            navigationService.UriFor<ManagerLoginViewModel>().Navigate();
        }
    }
}
