﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartClipBoard.Views;
using Caliburn.Micro;
using Microsoft.WindowsAzure.MobileServices;
using System.Windows;
using SmartClipBoard.Storage;
using SmartClipBoard.Handlers;
using SmartClipBoard.Factories;
using Microsoft.Phone.Controls;

namespace SmartClipBoard.ViewModels
{

    public class ManagerLoginViewModel : Screen
    {
        private INavigationService navigationService;

        private ManagerLoginView view;

        public ManagerLoginViewModel(INavigationService NavigationService)
        {
            this.navigationService = NavigationService;
        }

        protected override async void OnActivate()
        {
            base.OnActivate();
            this.view = GetView() as ManagerLoginView;
            
            await FirstLogin();
        }
        
        // TODO improve this method
        private async System.Threading.Tasks.Task FirstLogin()
        {    
            while (App.globalUser == null)
            {
                try
                {
                    await App.MobileService.LoginAsync(MobileServiceAuthenticationProvider.MicrosoftAccount).ContinueWith(t =>
                        {
                            if (t.IsFaulted)
                            {
                                Deployment.Current.Dispatcher.BeginInvoke(() => { MessageBox.Show("\nConnection failed!\n"); navigationService.UriFor<LoginScreenViewModel>().Navigate();});
                            }
                            else
                            {
                                App.globalUser = t.Result;
                            }
                        });
                }
                catch (InvalidOperationException)
                {
                   // Improve the Exception Handler  here..mainly when the user
                    // press the back
                    /*var result = MessageBox.Show("Do you want to return to the login screen?", "Attention!",
                                 MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() => { LoginScreenViewModel.navigate = true; navigationService.UriFor<LoginScreenViewModel>().Navigate(); });
                        navigationService.GoBack();
                        navigationService.RemoveBackEntry();
                    }*/

                    //Deployment.Current.Dispatcher.BeginInvoke(() => { LoginScreenViewModel.navigate = true; navigationService.UriFor<LoginScreenViewModel>().Navigate(); });

                    MessageBox.Show("InvalidOperationException");
                    //navigationService.GoBack();
                }
                finally
                {
                    if (App.globalUser != null)
                    {
                        UserData.SaveUser(App.globalUser);
                        Deployment.Current.Dispatcher.BeginInvoke(() => { MainViewModel.navigate = true; navigationService.UriFor<MainViewModel>().Navigate(); });
                    }

                }
            }
        }
    }
}