﻿using Caliburn.Micro;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Threading;
using Microsoft.WindowsAzure.MobileServices;
using SmartClipBoard.Views;
using SmartClipBoard.Models;
using SmartClipBoard.Services;
using SmartClipBoard.Storage;
using Microsoft.Phone.Notification;

namespace SmartClipBoard.ViewModels
{
    public class MainViewModel : Screen
    {
        private ItemService service;

        private INavigationService navigationService;
        
        private MainView view;

        private ApplicationBarIconButton refreshButton;

        private ApplicationBarMenuItem logoutMenuItem;

        public static bool navigate;

        private ObservableCollection<TodoItem> items;

        private TodoItem selectedItem;
        
        public MainViewModel(INavigationService NavigationService)
        {
            this.navigationService = NavigationService;
            service = new ItemService();

            App.CurrentChannel.ShellToastNotificationReceived += CurrentChannel_ShellToastNotificationReceived;
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            this.view = GetView() as MainView;
            this.refreshButton = (ApplicationBarIconButton)this.view.ApplicationBar.Buttons[0];
            this.logoutMenuItem = (ApplicationBarMenuItem)this.view.ApplicationBar.MenuItems[0];
            this.logoutMenuItem.Click += MenuItemLogout_Click;
            this.refreshButton.Click += ButtonRefresh_Click;      
            App.SetProgressIndicator();
            SystemTray.SetProgressIndicator(view, App.progressIndicator);
            RefreshAllItens();     
            while (navigationService.RemoveBackEntry() != null) ;
        }

        private static void CurrentChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            
            string item = e.Collection["wp:Text1"];

            Deployment.Current.Dispatcher.BeginInvoke(() => CopyToClipBoard(item));

        }

        public ObservableCollection<TodoItem> Items
        {
            get { return items; }
            set
            {
                items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }


        public TodoItem SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                NotifyOfPropertyChange(() => SelectedItem);
                CopyToClipBoard(value.Text);
            }
        }

        private static void CopyToClipBoard(string value) 
        {
            System.Diagnostics.Debug.WriteLine("CopyToClipBoard: {0}", value);

            if (value != null)
            {
                Clipboard.SetText(value);
                MessageBox.Show("Item copied to your clipboard !"); 
            }
        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            RefreshAllItens();
        }

        private void MenuItemLogout_Click(object sender, EventArgs e)
        {
            UserData.ClearUserData();
            Deployment.Current.Dispatcher.BeginInvoke(() => { LoginScreenViewModel.navigate = true; navigationService.UriFor<LoginScreenViewModel>().Navigate(); });
        }

        private async void RefreshAllItens()
        {
            try
            {
                App.SetProgressIndicator(true); 
                                
                List<TodoItem> todoList = await service.GetAllItems();

                Items = new BindableCollection<TodoItem>();
                
                foreach (TodoItem item in todoList)
                {
                    Items.Add(item);
                }
               
                App.SetProgressIndicator(false); 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error on loading itens :" + ex.Message);
            }
        }

    }
}